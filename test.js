const { binarySearch } = require('./index')
const { should, expect, assert } = require('chai')
should()

const array = [1, 2, 3, 3, 4, 5, 6, Number.MAX_VALUE]
const bad = [5, 1, 8, 13, 7, 42, 2]
const empty = []

describe('Binary search', function () {
  // empty array
  it('should throw \'Array can not be empty\' if input array is empty', function () {
    expect(() => binarySearch(empty, 22)).to.throw('Array can not be empty')
  })
  // not sorted array
  it('should throw \'Array must be sorted\' if input array is not sorted', function () {
    expect(() => binarySearch(bad, 22)).to.throw('Array must be sorted')
  })
  // wrong input types
  it('should throw \'Wrong input\' if array argument is not an array and element argument is not a number', function () {
    expect(() => binarySearch({}, 5)).to.throw('Wrong input')
    expect(() => binarySearch(1, 1)).to.throw('Wrong input')
    expect(() => binarySearch([1, 2, 3], {})).to.throw('Wrong input')
  })
  // no such element
  it('should return -1 if element is not present in input array', function () {
    binarySearch(array, 22).should.be.equal(-1)
  })
  // first
  it(`should return 0 if searching for ${array[0]} in array [${array}]`, function () {
    binarySearch(array, array[0]).should.be.equal(0)
  })
  // last
  it(`should return ${array.length - 1} if searching for ${array[array.length - 1]} in array [${array}]`, function () {
    binarySearch(array, array[array.length - 1]).should.be.equal(array.length - 1)
  })
  // return type
  it('should return number on correct inputs', function () {
    binarySearch(array, 5).should.be.a('number')
  })
})
