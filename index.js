function binarySearch(array, element) {
  if (!Array.isArray(array) || isNaN(element)) {
    throw new Error('Wrong input')
  }
  if (!isSorted(array)) {
    throw new Error('Array must be sorted')
  }
  if (!array.length) {
    throw new Error('Array can not be empty')
  }

  let left = 0
  let right = array.length - 1
  let middle
  let elIndex = -1

  while (left <= right) {
    middle = Math.floor(left + (right - left) / 2)
    if (element < array[middle]) {
      right = middle - 1
    }
    if (element > array[middle]) {
      left = middle + 1
    }
    if (element === array[middle]) {
      elIndex = middle
      break
    }
  }
  return elIndex
}

function isSorted(array) {
  for (let i = 1; i < array.length; i++) {
    if (array[i - 1] > array[i])
      return false
  }
  return true
}

module.exports = {
  binarySearch, isSorted
}
